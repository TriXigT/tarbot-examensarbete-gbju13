/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javametest;

/**
 *
 * @author Thorb
 */
public class MotorController {

    private Motor leftMotor;
    private Motor rightMotor;

    public MotorController() {
        rightMotor = new Motor(4, 17, 18);
        leftMotor = new Motor(27, 22, 23);
    }

   

    public void drive() {
        leftMotor.drive(Motor.Speed.FAST, Motor.Direction.FORWARD);
        rightMotor.drive(Motor.Speed.FAST, Motor.Direction.FORWARD);
    }

    public void turn(TurnDirection direction) {

        if (direction.equals(TurnDirection.LEFT)) {
            leftMotor.drive(Motor.Speed.MEDIUM, Motor.Direction.BACKWARD);
            rightMotor.drive(Motor.Speed.MEDIUM, Motor.Direction.FORWARD);

        } else if (direction.equals(TurnDirection.RIGHT)) {
            leftMotor.drive(Motor.Speed.MEDIUM, Motor.Direction.FORWARD);
            rightMotor.drive(Motor.Speed.MEDIUM, Motor.Direction.BACKWARD);
        }

    }

    public void reverse() {
        leftMotor.drive(Motor.Speed.FAST, Motor.Direction.BACKWARD);
        rightMotor.drive(Motor.Speed.FAST, Motor.Direction.BACKWARD);
    }

    public static enum TurnDirection {

        LEFT, RIGHT;
    }

}
