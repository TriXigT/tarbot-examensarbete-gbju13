/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javametest;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.midlet.MIDlet;

/**
 *
 * @author Pär & Thorb
 */
public class Main extends MIDlet {

    private MotorController motorController;
    private DistanceMeasure distanceMeasure;
    private boolean isGoing = false;
    private boolean turnDirection;
    private int turnCounter = 0;

    @Override
    public void startApp() {

        motorController = new MotorController();
        distanceMeasure = new DistanceMeasure();

        motorController.drive();
        isGoing = true;

        double distance = 0;
        while (true) {
            try {
                Thread.sleep(100);
                distance = distanceMeasure.measure();
            } catch (InterruptedException ex) {
                Logger.getLogger(Brain.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(distance);
            if (distance < 20) {
                System.out.println("dist < 20");
                if (distance < 10) {
                    System.out.println("dist < 10");
                    motorController.reverse();
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Brain.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                motorController.turn(getTurnDirection());
                isGoing = false;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Brain.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (isGoing == false) {

                motorController.drive();
            }

        }

    }

    public MotorController.TurnDirection getTurnDirection() {

        if (turnDirection == false) {
             turnCounter++;
             if(turnCounter >= 3){
                 turnCounter = 0;
                 turnDirection = !turnDirection;
             }
            return MotorController.TurnDirection.LEFT;
        } else {
             turnCounter++;
             if(turnCounter >= 3){
                 turnCounter = 0;
                 turnDirection = !turnDirection;
             }
            return MotorController.TurnDirection.RIGHT;
        }
        
       
    }

    @Override
    public void destroyApp(boolean unconditional) {
        System.out.println("app going down");
    }
}
