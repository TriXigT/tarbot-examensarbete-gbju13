/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javametest;

import java.util.HashMap;
import java.util.Map;
import javax.microedition.midlet.MIDlet;

/**
 *
 * @author Thorb & Pär
 */
public class SpeedTest extends MIDlet {

    private final int ITERATIONS = 10000;

    private Map<Long, Integer> measures = new HashMap<Long, Integer>();
    private long currentMeasure = 0;

    @Override
    public void startApp() {
        for (int i = 0; i < ITERATIONS; i++) {
            currentMeasure = System.nanoTime() - System.nanoTime();

            updateMeasures(Math.round(currentMeasure / 1000) * 1000);

            System.out.println(currentMeasure);
        }

        System.out.println();

        printMeasures();
        printMeasuresAverage();
    }

    private void updateMeasures(long measure) {
        for (Map.Entry<Long, Integer> entry : measures.entrySet()) {
            if (entry.getKey() == measure) {
                entry.setValue(entry.getValue() + 1);
                return;
            }
        }

        measures.put(measure, 1);
    }

    private void printMeasures() {
        for (Map.Entry<Long, Integer> entry : measures.entrySet()) {
            System.out.println(String.format("Measure [%d] occured [%d] times", entry.getKey(), entry.getValue()));
        }
    }

    private void printMeasuresAverage() {
        double measureSum = 0;

        for (Map.Entry<Long, Integer> entry : measures.entrySet()) {
            measureSum += entry.getKey() * entry.getValue();
        }

        System.out.println(String.format("Average execution time [%d]", Math.round(measureSum / ITERATIONS)));
        
    }

    @Override
    public void destroyApp(boolean unconditional) {
    }
}
