/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javametest;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;

/**
 *
 * @author Pär & Thorb
 */
public class DistanceMeasure{

    private final int PULSE_LENGTH_IN_NANOSECONDS = 10000;
    private final int SPEED_OF_SOUND = 34029;

    private GPIOPin triggerPin = null;
    private GPIOPin echoPin = null;

    public DistanceMeasure() {
        
        try {
            triggerPin = (GPIOPin) DeviceManager.open(new GPIOPinConfig(0, Constants.PIN_HCSR04_TRIGGER,
                    GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                    GPIOPinConfig.TRIGGER_NONE, false));

            echoPin = (GPIOPin) DeviceManager.open(new GPIOPinConfig(0, Constants.PIN_HCSR04_ECHO,
                    GPIOPinConfig.DIR_INPUT_ONLY, GPIOPinConfig.MODE_INPUT_PULL_UP,
                    GPIOPinConfig.TRIGGER_NONE, false));
        } catch (IOException ex) {
            Logger.getLogger(DistanceMeasure.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

   
    public double measure() {

          return pulse();
        
    }

    public double pulse() {
        long distance = 0;
        try {
            triggerPin.setValue(true);
            Thread.sleep(0, PULSE_LENGTH_IN_NANOSECONDS);
            triggerPin.setValue(false);
            long starttime = System.nanoTime();
            long stop = starttime;
            long start = starttime;

            while ((echoPin.getValue() == false) && (start < starttime + 1000000000L * 2)) {
                start = System.nanoTime();
            }
            
            while ((echoPin.getValue() == true) && (stop < starttime + 1000000000L * 2)) {
                stop = System.nanoTime();
            }
            
            long difference = (stop - start);
            distance = difference * SPEED_OF_SOUND;
        } catch (Exception ex) {
            Logger.getLogger(DistanceMeasure.class.getName()).log(Level.WARNING, ex.getMessage());
        }
        return distance / 2.0 / (1000000000L);
    }
}
