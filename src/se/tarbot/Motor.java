/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javametest;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.dio.DeviceManager;
import jdk.dio.gpio.GPIOPin;
import jdk.dio.gpio.GPIOPinConfig;

/**
 *
 * @author Thorb & Pär
 */
public class Motor {

    private GPIOPin forwardPin;
    private GPIOPin backwardPin;
    private GPIOPin PWMPin;
    private Thread pwmThread;
    private Speed speed;
    private boolean isDriving = false;

    public Motor(int forwardPinNumber, int backwardPinNumber, int PWMPinNumber) {

        pwmThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {
                    while (isDriving) {
                        try {
                            Thread.sleep(speed.getPauseLength());
                            PWMPin.setValue(true);
                            Thread.sleep(speed.getPulseLength());
                            PWMPin.setValue(false);

                        } catch (Exception ex) {
                            Logger.getLogger(Motor.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        });
        pwmThread.start();
        try {

            forwardPin = (GPIOPin) DeviceManager.open(new GPIOPinConfig(0, forwardPinNumber,
                    GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                    GPIOPinConfig.TRIGGER_NONE, false));

            backwardPin = (GPIOPin) DeviceManager.open(new GPIOPinConfig(0, backwardPinNumber,
                    GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                    GPIOPinConfig.TRIGGER_NONE, false));

            PWMPin = (GPIOPin) DeviceManager.open(new GPIOPinConfig(0, PWMPinNumber,
                    GPIOPinConfig.DIR_OUTPUT_ONLY, GPIOPinConfig.MODE_OUTPUT_PUSH_PULL,
                    GPIOPinConfig.TRIGGER_NONE, false));

        } catch (IOException ex) {
            Logger.getLogger(Motor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void drive(Speed speed, Direction direction) {

        this.speed = speed;

        if (direction.equals(Direction.FORWARD)) {
            try {
                forwardPin.setValue(true);
                backwardPin.setValue(false);
            } catch (Exception ex) {
                Logger.getLogger(Motor.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (direction.equals(Direction.BACKWARD)) {
            try {
                forwardPin.setValue(false);
                backwardPin.setValue(true);
            } catch (Exception ex) {
                Logger.getLogger(Motor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        isDriving = true;
    }

    public void stop() {
        isDriving = false;
    }

    public static enum Direction {

        FORWARD, BACKWARD;
    }

    public static enum Speed {

        LOW(10, 2), MEDIUM(10, 10), FAST(0, 10);

        private int pauseLength;
        private int pulseLength;

        private Speed(int pauseLength, int pulseLength) {
            this.pauseLength = pauseLength;
            this.pulseLength = pulseLength;
        }

        public int getPauseLength() {
            return pauseLength;
        }

        public int getPulseLength() {
            return pulseLength;
        }

    }

}
